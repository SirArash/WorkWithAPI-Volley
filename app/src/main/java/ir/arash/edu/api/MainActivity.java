package ir.arash.edu.api;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements ApiService.OnIPInfoReceived{

    Button button;
    private ApiService apiService;
    private TextView IP;
    private TextView information;
    private ProgressBar ProgBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = findViewById(R.id.MyBTN);
        IP=findViewById(R.id.lbl_userIP);
        information=findViewById(R.id.lbl_useripINFO);
        ProgBar=findViewById(R.id.ProgBar);
        apiService= new ApiService(this);

        DoWork();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DoWork();
            }
        });
    }

    private void DoWork(){
        apiService.getIPData(MainActivity.this);
        ProgBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void OnReceived(IPinfo IPinfo) {
        if(IPinfo != null){

            IP.setText(IPinfo.getIPquery());
            information.setText(IPinfo.getIPcountryCode() +", " + IPinfo.getIPcity());

        } else{
            Toast.makeText(this, "خطا در دریافت اطلاعات!", Toast.LENGTH_SHORT).show();
        }
        ProgBar.setVisibility(View.INVISIBLE);
    }
}