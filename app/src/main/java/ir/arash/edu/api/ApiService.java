package ir.arash.edu.api;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

public class ApiService {


    private Context context;
    public ApiService(Context context){ this.context = context; }

    public void getIPData(final OnIPInfoReceived OnIPInfoReceived){
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, "http://ip-api.com/json"
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("MyApp", "onResponse: " + response.toString());
                OnIPInfoReceived.OnReceived(parsRespons(response));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("MyAppErr", "onErrorResponse: " + error.toString());
                OnIPInfoReceived.OnReceived(null);
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(8000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(context);
        rQueue.add(request);
    }

    private IPinfo parsRespons(JSONObject response){
        try {

            IPinfo ipinfo = new IPinfo();
            //JSONArray JsonArray=response.getJSONArray("0");
            //JSONObject JsonObject = response.getJSONObject("address_format");
            ipinfo.setIPquery(response.getString("query"));
            ipinfo.setIPcountryCode(response.getString("countryCode"));
            ipinfo.setIPcity(response.getString("city"));
            return  ipinfo;

        } catch (JSONException e) {
            Log.e("JSONErr", "JSONException: ",e);
            return null;
        }
    }

    public  interface  OnIPInfoReceived{
        void OnReceived(IPinfo IPinfo);
    }
}
