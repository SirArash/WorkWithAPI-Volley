package ir.arash.edu.api;

public class IPinfo {

    private String IPquery;
    private String IPcity;
    private String IPcountryCode;

    public String getIPquery() {
        return IPquery;
    }

    public void setIPquery(String IPquery) {
        this.IPquery = IPquery;
    }

    public String getIPcity() {
        return IPcity;
    }

    public void setIPcity(String IPcity) {
        this.IPcity = IPcity;
    }

    public String getIPcountryCode() {
        return IPcountryCode;
    }

    public void setIPcountryCode(String IPcountryCode) {
        this.IPcountryCode = IPcountryCode;
    }
}
